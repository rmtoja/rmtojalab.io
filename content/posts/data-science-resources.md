Title: Data Science resources
Date: 2023-09-03
Category: Data Science
Tags: machine learning, deep learning
Slug: data-science-resources
Author: Ramiro Moreira Toja
Summary: A collection on courses and books on machine learning, deep learning, statistics and data-viz
lang: en


This is a dump of open tabs and bookmarks I've been gathering for some time. Maybe I'll make it pretty some day, or add better info and my own reccomendations.

## Courses

### FastAI

Practical Deep Learning for Coders & From Deep Learning Foundations to Stable Diffusion

<https://course.fast.ai/>
<https://course.fast.ai/Lessons/part2.html>

Someone at Hacker News reccomended to also check out part 2 of the v3 (2019) version of the course. It's pre-Stable Diffusion, but he says it explains core concepts slightly better.

<https://course19.fast.ai/part2>


### Neural Networks: Zero to Hero (Karpathy)

Andrej Karpathy's course. I think it uses his own framework, micrograd (which inspired derivatives, s GeoHot/CommaAI 's tinygrad)

<https://karpathy.ai/zero-to-hero.html>

### An Introduction to Statistical Learning (Stanford - James, Witten, HAstie, Tibshirani, Taylor)

Book in R (2nd Ed 2021) or in Python (1st Ed 2023).
Course in EDX, videos in Youtube (R only, Python version of course willbe available in 2024)

<https://www.statlearning.com/online-course>
<https://www.edx.org/learn/statistics/stanford-university-statistical-learning>
<https://www.youtube.com/playlist?list=PLoROMvodv4rOzrYsAxzQyHb8n_RWNuS1e>

### Deep Learning Book (MIT Press - Goodfellow, Bengio, Courville)

Web book available, exercises and lecture slides available.
Seems to be heavy on math.

<https://www.deeplearningbook.org/>

### Escuela de Datos Vivos

Paid courses (mostly in USD, some in ARS), book available. Blog with RSS feed.

<https://www.escueladedatosvivos.ai/>
<https://blog.escueladedatosvivos.ai/>
<https://librovivodecienciadedatos.ai/>

### Deep Learning Systems

<https://dlsyscourse.org/lectures/>

### Deep Learning at the Vrije Universiteit Amsterdam (DLVU)

<https://dlvu.github.io/>

### DEEP LEARNING (NYU)

Requires some previous knowledge on ML

<https://atcold.github.io/pytorch-Deep-Learning/>


### The Illustrated Stable Diffusion

<https://jalammar.github.io/illustrated-stable-diffusion/>
<https://www.youtube.com/channel/UCmOwsoHty5PrmE-3QhUBfPQ/videos>

### Cognitive Class

Free courses on AI, Data Science, and sysops

<https://cognitiveclass.ai/>
<https://cognitiveclass.ai/learn>




## Books (with no related courses available)

### Machine Learning with PyTorch and Scikit-Learn

Packt 2022
<https://sebastianraschka.com/blog/2022/ml-pytorch-book.html>

### Probabilistic Machine Learning: An Introduction & Advanced Topics

<https://probml.github.io/pml-book/book1.html>
<https://probml.github.io/pml-book/book2.html>


### Metacademy

Roadmaps and courses on ML and statistics (seems to be broken as of 2023-09-03).
<https://metacademy.org/>

Level-Up Your Machine Learning (Metacademy): blog post recommending books
<https://www.metacademy.org/roadmaps/cjrd/level-up-your-ml>

Beginners: Machine Learning with R (Lantz)
Intermediate: Pattern Recognition and Machine Learning (Bishop)


## Math for ML and DL

### Linear Algebra for programmers, part 1

<https://coffeemug.github.io/spakhm.com/posts/01-lingalg-p1/linalg-p1.html>

### The Matrix Calculus You Need For Deep Learning (Parr, Howard)

<https://explained.ai/matrix-calculus/>



## Resources with a focus on landing a job

### LLM Utils - AI Learning Curation

Reccomendations on resources depending on which kind of role you're trying to get: Software Engineer, Machine Learning Engineer, Research Engineer, Research Scientist

<https://llm-utils.org/AI+Learning+Curation>

### Ace the Data Science Interview/Job Hunt

<https://www.acethedatascienceinterview.com/>
<https://ace-the-data-science-interview.teachable.com/>
<https://www.acethedatascienceinterview.com/pdf-download-of-ace-the-data-science-interview-ebook>


# Statistics

### Brandon Foltz

<https://www.bcfoltz.com/stats-101/>
<https://www.youtube.com/watch?v=fyRubPKgVoY>

### Andrés Farall

Bayesian statistics, ML and DL

<https://www.youtube.com/channel/UC5Rup8Tq90zOekekkNlSdRQ/playlists>


### The Probability and Statistics Cookbook

<http://statistics.zone/>

### Statistical Rethinking

Robert McElreath

<https://github.com/rmcelreath/stat_rethinking_2022>
<https://www.youtube.com/playlist?list=PLDcUM9US4XdMROZ57-OIRtIK0aOynbgZN>

### High-Dimensional Probability and Applications in Data Science

<https://www.math.uci.edu/~rvershyn/teaching/hdp/hdp.html>


# Data Viz

### The Python Graph Gallery

<https://python-graph-gallery.com/>

### From Data to Viz

<https://www.data-to-viz.com/>
<https://www.data-to-viz.com/caveats.html>
