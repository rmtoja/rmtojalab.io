Title: Como instalar Debian con un escritorio KDE Plasma ligero
Date: 2022-09-23
Category: Linux
Tags: linux, debian, kde
Slug: debian-kde-ligero
Author: Ramiro Moreira Toja
Summary: Usando la imagen netinstall de Debian, podemos obtener un escritorio KDE mínimo.
Status: draft

Netinstall ISO >>> Tasksel: solo dejar marcado "Standard system utilities"

Reboot >> sudo apt install kde-plasma-desktop plasma-nm
