Title: Acerca de
Date: 2023-09-03
Category: about
Tags: about
Slug: about
Author: Ramiro Moreira Toja
Summary: Acerca de


"Una vez me pregunté si todo esto tenía sentido, si mi misión en el mundo no era otra. Pensé muy seriamente en dejar todo e ir a trabajar con los pobres al África. Al final no fui por el calor. Yo sufro mucho el calor." - Amalia Lacroze de Fortabat, 1998
