#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from datetime import date

AUTHOR = 'Ramiro Moreira Toja'
SITENAME = 'La página personal de Ramiro'
SITETITLE = 'Ramiro Moreira Toja' #Flex theme config
SITESUBTITLE = 'Donde le dejo notas al Ramiro del futuro. Quizás un blog'
SITEURL = 'https://rmtoja.gitlab.io'

#Content
PATH = 'content'
PAGE_PATHS = ['pages']
ARTICLE_PATHS = ['posts']
DEFAULT_CATEGORY = 'misc'

#Theme
THEME = '/tmp/Flex'
#Flex theme configs
THEME_COLOR = 'dark'
THEME_COLOR_AUTO_DETECT_BROWSER_PREFERENCE = True

#Localization
TIMEZONE = 'America/Buenos_Aires'
DEFAULT_DATE_FORMAT = '%Y-%m-%d'
DEFAULT_LANG = 'es'
LOCALE = 'es_AR'

#URL settings
ARTICLE_URL = 'blog/{date:%Y}/{date:%m}/{date:%d}/{slug}/'
ARTICLE_SAVE_AS = 'blog/{date:%Y}/{date:%m}/{date:%d}/{slug}/index.html'
PAGE_URL = '{slug}/'
PAGE_SAVE_AS = '{slug}/index.html'
#ARCHIVES_SAVE_AS = 'archives.html'

# Feed generation is usually not desired when developing
FEED_DOMAIN = SITEURL + '/blog/'
FEED_ALL_ATOM = 'feeds/atom.xml'
FEED_ALL_RSS = 'feeds/rss.xml'
RSS_FEED_SUMMARY_ONLY = False
#FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
#LINKS = (('Pelican', 'https://getpelican.com/'),
#         ('Python.org', 'https://www.python.org/'),
#         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
#         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('gitlab', 'http://gitlab.com/rmtoja'),
          ('linkedin', 'https://www.linkedin.com/in/rmoreiratoja/'),
          ('rss', 'feeds/all.atom.xml')
          )

#Flex theme menu
MAIN_MENU = True
MENUITEMS = (
    ("Archivo", "/archives.html"),
    ("Categorías", "/categories.html"),
    )
#    ("Tags", "/tags.html"),
#)


#Copyright/Creative Commons
COPYRIGHT_YEAR = date.today().year
CC_LICENSE = { #Flex theme config
    "name": "Creative Commons Attribution-ShareAlike",
    "version": "4.0",
    "slug": "by-sa"
}


DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# custom page generated with a jinja2 template
#TEMPLATE_PAGES = {'pages/cv.html': 'resume.html'}